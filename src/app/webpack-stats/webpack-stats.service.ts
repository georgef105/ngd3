import { Injectable } from '@angular/core';
import { GraphData } from '../graphs/graph-data.interface';
import { Stats } from 'webpack';
import { WebpackMapperOptions } from './webpack-stats.mapper';

@Injectable({
  providedIn: 'root'
})
export class WebpackStatsService {
  public process(stats: Stats.ToJsonOutput, type: 'module', id: string, options: WebpackMapperOptions): Promise<GraphData> {
    if (typeof Worker !== 'undefined') {
      const worker = new Worker('./webpack-stats.worker', { type: 'module' });

      return new Promise<GraphData>((resolve, reject) => {
        worker.onmessage = ({ data }) => {
          resolve(data);
        };
        worker.onerror = error => reject(error);
        const message = {
          worker: './webpack-stats.worker',
          payload: stats,
          type,
          id,
          options
        };
        worker.postMessage(message);
      }).finally(() => {
        console.log('terminating webpack worker');
        worker.terminate();
      });
    }

    return Promise.reject('webworker not available');
  }
}
