import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebpackStatsRoutingModule } from './webpack-stats-routing.module';
import { WebpackStatsComponent } from './webpack-stats.component';
import { TreeModule } from '../graphs/tree/tree.module';

@NgModule({
  declarations: [WebpackStatsComponent],
  imports: [
    CommonModule,
    WebpackStatsRoutingModule,
    TreeModule
  ]
})
export class WebpackStatsModule { }
