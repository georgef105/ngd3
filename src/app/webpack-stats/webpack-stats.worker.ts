/// <reference lib="webworker" />
import { mapToGraphData } from './webpack-stats.mapper';

addEventListener('message', ({ data }) => {
  const { payload, worker, type, id, options } = data;
  if (worker !== './webpack-stats.worker') {
    return;
  }
  postMessage(mapToGraphData(payload, type, id, options));
  // postMessage(data);
});
