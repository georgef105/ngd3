import { GraphData } from '../graphs/graph-data.interface';
import { Stats } from 'webpack';
import { Link } from '../d3/models/link';
import { Node } from '../d3/models/node';

const MAX_DEPTH = 4;
const MAX_NODES = 100;

export interface WebpackMapperOptions {
  maxDepth: number;
  lookUpType: 'children' | 'parents';
  maxNodes: number;
}

export function mapToGraphData(stats: Stats.ToJsonOutput, type: 'module', id: string, options: WebpackMapperOptions): GraphData {
  const itemToUse = stats.modules.find(module => module.id === id)
    || stats.modules[0];

  const getLinkedItems = options.lookUpType === 'parents'
    ? item => item.reasons.map(reason => stats.modules.find(module => module.id === reason.moduleId))
    : item => stats.modules.filter(module => (<any>module.reasons as any[]).some(reason => reason.moduleId === item.id));

  return getNodesAndLinks(itemToUse, getLinkedItems, options);
  // const nodes = getNodes(stats);
  // const links = getLinks(stats, nodes);
  // return {
  //   nodes,
  //   links
  // };
}

function getNodesAndLinks(
  item: Stats.FnModules,
  getLinkedItems: (item: Stats.FnModules) => Stats.FnModules[],
  options: WebpackMapperOptions,
  parentNode?: Node,
  depth: number = 0,
  graphData: GraphData = { nodes: [], links: [] }): GraphData {
  if (depth > options.maxDepth || graphData.nodes.length > options.maxNodes || item.name.includes('node_modules')) {
    return;
  }

  const itemNode = createNodeFromItem(item);
  graphData.nodes.push(itemNode);

  if (parentNode) {
    graphData.links.push(new Link(parentNode, itemNode));
  }

  getLinkedItems(item).forEach(linkedItem => getNodesAndLinks(linkedItem, getLinkedItems, options, itemNode, depth + 1, graphData));

  return graphData;
}

function createNodeFromItem(item): Node {
  const node = new Node(item.id, 20);
  node.x = 0;
  node.y = 0;
  node.name = item.name;
  return node;
}

function getNodes(stats: Stats.ToJsonOutput): Node[] {
  return stats.chunks.slice(0, 20).map(chunk => {
    const node = new Node(chunk.id.toString(), 20); // chunk.size / 10000);
    node.x = 0;
    node.y = 0;
    return node;
  });
}

function getLinks(stats: Stats.ToJsonOutput, nodes: Node[]): Link[] {
  return (stats.chunks as Array<any>).slice(0, 20).reduce((links: Link[], chunk) => {
    const sourceNode = nodes.find(node => node.id === chunk.id.toString());
    const chunkLinks = sourceNode ? chunk.parents
      .map(parent => nodes.find(node => node.id === parent.toString()))
      .filter(Boolean)
      .map(targetNode => new Link(sourceNode, targetNode)) : [];

    return [
      ...links,
      ...chunkLinks
    ];
  }, []);
}