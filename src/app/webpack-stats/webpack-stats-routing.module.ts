import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WebpackStatsComponent } from './webpack-stats.component';

const routes: Routes = [
  {
    path: 'tree',
    children: [
      {
        path: 'module',
        component: WebpackStatsComponent,
        data: {
          collectionType: 'module'
        }
      },
      {
        path: 'module/:id',
        component: WebpackStatsComponent,
        data: {
          collectionType: 'module'
        }
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'module'
      }
    ]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'tree'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebpackStatsRoutingModule { }
