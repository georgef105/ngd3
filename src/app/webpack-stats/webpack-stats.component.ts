import { Component, OnInit } from '@angular/core';
import { UploaderDataService } from '../uploader/uploader-data.service';
import { Observable, combineLatest, Subject, BehaviorSubject, of } from 'rxjs';
import { GraphData } from '../graphs/graph-data.interface';
import { filter, switchMap, map, debounceTime } from 'rxjs/operators';
import { WebpackStatsService } from './webpack-stats.service';
import { Stats } from 'webpack';
import { ActivatedRoute } from '@angular/router';
import { WebpackMapperOptions } from './webpack-stats.mapper';

@Component({
  selector: 'app-webpack-stats',
  templateUrl: './webpack-stats.component.html',
  styleUrls: ['./webpack-stats.component.scss']
})
export class WebpackStatsComponent implements OnInit {
  public stats$: Observable<Stats.ToJsonOutput>;
  public graphData$: Observable<GraphData>;

  public currentCollection$: Observable<Stats.FnModules[]>;

  private searchInput$ = new BehaviorSubject<string>('');

  private lookUpType$ = new BehaviorSubject<'children' | 'parents'>('children');
  private maxDepth$ = new BehaviorSubject<number>(2);
  private maxNodes$ = new BehaviorSubject<number>(100);

  constructor(
    private webpackStatsService: WebpackStatsService,
    private uploaderDataService: UploaderDataService<Stats.ToJsonOutput>,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.stats$ = this.uploaderDataService.getFileData().pipe(
      filter(Boolean)
    );

    const id$ = this.activatedRoute.params.pipe(
      map(params => params.id)
    );

    const mapperOptions$: Observable<WebpackMapperOptions> = combineLatest(
      this.lookUpType$,
      this.maxDepth$,
      this.maxNodes$).pipe(
      map(([lookUpType, maxDepth, maxNodes]) => {
        return {
          maxDepth,
          maxNodes,
          lookUpType
        };
      }),
      debounceTime(500)
    );

    this.graphData$ = combineLatest(this.stats$, id$, mapperOptions$).pipe(
      switchMap(([jsonData, id, options]) => this.webpackStatsService.process(jsonData, 'module', id, options))
    );

    this.currentCollection$ = combineLatest(this.stats$, this.searchInput$).pipe(
      map(([stats, searchInput]) => {
        return stats.modules.filter(module => {
          const isExcluded = module.name.includes('node_module');
          const isInSearch = searchInput
            ? module.name.includes(searchInput)
            : true;

          return !isExcluded && isInSearch;
        });
      })
    );
  }

  public onSearchInput(searchInput: string): void {
    this.searchInput$.next(searchInput);
  }

  public onLookUpOptionChange(value: boolean): void {
    this.lookUpType$.next(value ? 'parents' : 'children');
  }

  public onMaxDepthOptionChange(value: number): void {
    this.maxDepth$.next(value);
  }

  public onMaxNodesOptionChange(value: number): void {
    this.maxNodes$.next(value);
  }

}
