import { TestBed } from '@angular/core/testing';

import { WebpackStatsService } from './webpack-stats.service';

describe('WebpackStatsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WebpackStatsService = TestBed.get(WebpackStatsService);
    expect(service).toBeTruthy();
  });
});
