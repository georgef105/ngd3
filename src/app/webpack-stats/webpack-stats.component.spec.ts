import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebpackStatsComponent } from './webpack-stats.component';

describe('WebpackStatsComponent', () => {
  let component: WebpackStatsComponent;
  let fixture: ComponentFixture<WebpackStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebpackStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebpackStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
