import { TestBed } from '@angular/core/testing';

import { UploaderDataService } from './uploader-data.service';

describe('UploaderDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploaderDataService<any> = TestBed.get(UploaderDataService);
    expect(service).toBeTruthy();
  });
});
