import { Component } from '@angular/core';
import { UploaderDataService } from './uploader-data.service';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent {

  constructor(
    private uploaderDataService: UploaderDataService<any>
  ) { }

  public upload(event): void {
    const { files } = event.target;

    if (files.length) {
      const reader = new FileReader();
      reader.onload = this.onFileUploaded();

      reader.readAsText(files[0]);
    }
  }

  private onFileUploaded(): any {
    return event => {
      const { result } = event.target;

      this.parseToJson(result).then(json => {
        return this.uploaderDataService.setFileData(json);
      });
    };
  }

  private parseToJson(input: string): Promise<object> {
    if (typeof Worker !== 'undefined') {
      const worker = new Worker('./uploader.worker', { type: 'module' });

      return new Promise((resolve, reject) => {
        worker.onmessage = ({ data }) => resolve(data);
        worker.onerror = err => reject(err);
        worker.postMessage(input);
      }).then(data => {
        console.log('terminating uploader worker');
        worker.terminate();
        return data;
      });
    }

    return Promise.resolve(JSON.parse(input));
  }
}
