import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploaderDataService<T> {
  private fileData$ = new BehaviorSubject<T>(null);

  public setFileData(data: T): void {
    this.fileData$.next(data);
  }

  public getFileData(): Observable<T> {
    return this.fileData$;
  }
}
