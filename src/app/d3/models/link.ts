import { Node } from './node';
// import * as d3 from 'd3';

// Implementing SimulationLinkDatum interface into our custom Link class
export class Link { // implements d3.SimulationLinkDatum<Node> {
    public index?: number;

    constructor(
      public source: Node,
      public target: Node
    ) { }
}
