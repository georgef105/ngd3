// import { SimulationNodeDatum } from 'd3';

export class Node { // implements SimulationNodeDatum {
  public index?: number;
  public x?: number;
  public y?: number;
  public vx?: number;
  public vy?: number;
  public fx?: number | null;
  public fy?: number | null;

  public name?: string;

  public radius: number;
  public fontSize: number;

  constructor(
    public id: string,
    private size: number = 10
  ) {
    this.radius = size;
    this.fontSize = size * 0.7;
  }
}
