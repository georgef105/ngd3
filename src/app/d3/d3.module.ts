import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZoomableDirective } from './directives/zoomable.directive';
import { DraggableDirective } from './directives/draggable.directive';

@NgModule({
  declarations: [ZoomableDirective, DraggableDirective],
  imports: [
    CommonModule
  ],
  exports: [
    ZoomableDirective,
    DraggableDirective
  ]
})
export class D3Module { }
