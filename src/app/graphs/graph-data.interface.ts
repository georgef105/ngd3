import { Link } from '../d3/models/link';
import { Node } from '../d3/models/node';
export interface GraphData {
  nodes: Node[];
  links: Link[];
}
