import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TreeRoutingModule } from './tree-routing.module';
import { TreeComponent } from './tree.component';
import { NodeVisualModule } from '../shared/node-visual/node-visual.module';
import { LinkVisualModule } from '../shared/link-visual/link-visual.module';
import { D3Module } from 'src/app/d3/d3.module';

@NgModule({
  declarations: [TreeComponent],
  imports: [
    CommonModule,
    TreeRoutingModule,
    NodeVisualModule,
    LinkVisualModule,
    D3Module
  ],
  exports: [
    TreeComponent
  ]
})
export class TreeModule { }
