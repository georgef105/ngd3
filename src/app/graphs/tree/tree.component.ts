import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnChanges, SimpleChanges, Input } from '@angular/core';
import { D3Service } from 'src/app/d3/d3.service';
import { ForceDirectedGraph, Link, Node, Options } from 'src/app/d3/models';
import { filter, map, tap, switchMap, shareReplay } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { GraphData } from '../graph-data.interface';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TreeComponent implements OnInit, OnChanges {
  @Input() public graphData: GraphData;

  public graph$: Observable<ForceDirectedGraph>;

  public nodes: Node[] = [];
  public links: Link[] = [];
  public options: Options = {
    width: 800,
    height: 600
  };

  private graphData$ = new Subject<GraphData>();

  constructor(
    private cdr: ChangeDetectorRef,
    private d3Service: D3Service
  ) { }

  public ngOnInit() {
    this.graph$ = this.graphData$.pipe(
      filter(Boolean),
      map(data => this.d3Service.getForceDirectedGraph(data.nodes, data.links, this.getOptions())),
      tap(graph => graph.initSimulation(this.options)),
      shareReplay()
    );

    this.graph$.pipe(
      switchMap(graph => graph.ticker)
    ).subscribe(() => {
      this.cdr.markForCheck();
    });
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.graphData) {
      this.graphData$.next(this.graphData);
    }
  }

  private getOptions(): Options {
    return {
      width: window.innerWidth,
      height: window.innerHeight
    };
  }
}
