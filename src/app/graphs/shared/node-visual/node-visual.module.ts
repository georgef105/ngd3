import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NodeVisualComponent } from './node-visual.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NodeVisualComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    NodeVisualComponent
  ]
})
export class NodeVisualModule { }
