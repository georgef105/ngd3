import { Component, Input } from '@angular/core';
import { Node } from 'src/app/d3/models';

@Component({
// tslint:disable-next-line: component-selector
  selector: '[nodeVisual]',
  templateUrl: './node-visual.component.html',
  styleUrls: ['./node-visual.component.scss']
})
export class NodeVisualComponent {
// tslint:disable-next-line: no-input-rename
  @Input('nodeVisual') public node: Node;
}
