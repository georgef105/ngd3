import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkVisualComponent } from './link-visual.component';

@NgModule({
  declarations: [LinkVisualComponent],
  imports: [
    CommonModule
  ],
  exports: [
    LinkVisualComponent
  ]
})
export class LinkVisualModule { }
