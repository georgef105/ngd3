import { Component, Input } from '@angular/core';
import { Link } from 'src/app/d3/models';

@Component({
// tslint:disable-next-line: component-selector
  selector: '[linkVisual]',
  templateUrl: './link-visual.component.html',
  styleUrls: ['./link-visual.component.scss']
})
export class LinkVisualComponent {
// tslint:disable-next-line: no-input-rename
  @Input('linkVisual') public link: Link;
}
