import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'webpack',
    loadChildren: './webpack-stats/webpack-stats.module#WebpackStatsModule'
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'webpack'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
